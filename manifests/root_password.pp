# Create the file /etc/mysql/password.root containing the root password
# for other scripts to use.
define mysql::root_password(
  $wallet_name        = $name,
  $root_password_file = '/etc/mysql/password.root'
){

  if (!$wallet_name) {
    crit('missing wallet_name')
  }

  # The resulting file should simple be a single line containing the password.

  base::wallet { $wallet_name:
    ensure  => present,
    path    => $root_password_file,
    type    => 'file',
    owner   => root,
    group   => root,
    mode    => '0640',
  }

}
