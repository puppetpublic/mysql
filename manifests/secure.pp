## allow stunnel connections to the server, if requested
class mysql::secure inherits ::stunnel {
  # Install .pem and cert hash
  selfsign_cert { 'mysql-stunnel.pem':
    ensure => present,
    days   => 730,
  }

  case $::operatingsystem {
    'redhat': {
      file {
        '/etc/ssl':       ensure => directory;
        '/etc/ssl/certs': ensure => directory;
      }
      file { '/etc/init.d/mysql-stunnel':
        source => 'puppet:///modules/mysql/etc/init.d/mysql-stunnel',
        mode   => '0755';
      }

      service { 'mysql-stunnel':
        enable     => true,
        hasrestart => true,
        hasstatus  => false,
        require    => File['/etc/init.d/mysql-stunnel'],
      }
      file { '/etc/stunnel/mysql.conf':
        source => 'puppet:///modules/mysql/etc/stunnel/mysql-stunnel.conf.redhat'
      }
      file { '/etc/newsyslog.daily/mysql-stunnel':
        source => 'puppet:///modules/mysql/etc/newsyslog.daily/mysql-stunnel';
      }
    }

    'debian', 'ubuntu': {
      file { '/etc/stunnel/mysql-stunnel.conf':
        source => 'puppet:///modules/mysql/etc/stunnel/mysql-stunnel.conf';
      }
      base::daemontools::supervise { 'mysql-stunnel':
        ensure => present,
        source => 'puppet:///modules/mysql/service/mysql-stunnel/run';
      }
      file { '/etc/newsyslog.daily/mysql-stunnel':
        source => 'puppet:///modules/mysql/etc/newsyslog.daily/mysql-stunnel.debian';
      }
    }

    default: {
      fail("no case for operatingsystem '${::operatingsystem}'")
    }

  }
}
