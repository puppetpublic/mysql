# modules/mysql/manifests/server/ssl.pp
#
# Enable MySQL to use built-in SSL-based encryption for secure connections
# (if stunnel is not available or inappropriate).
#
#
# Syntax:
#
#    mysql::server::ssl { "<hostname>":
#      ensure   => present,
#      subject  => "<hostname>",
#      keyname  => "unix-<hostname>-ssl-key",
#      symlink  => false,
#    }
#
# This will download a public/private key pair and set up mysqld to use
# this key-pair when making SSL connections. The public certificate should be
# put into the usual location in puppet (modules/apache/files/certs). The
# private key must be stored in wallet. The wallet name defaults to
# "unix-<hostname>-ssl-key" (THIS NAME NEEDS TO CHANGE: see
# http://www.eyrie.org/~eagle/software/wallet/naming.html).
#
# A symlink /etc/ssl/certs/mysql-server.pem will be created pointing at the
# public certificate. Likewise, a symlink /etc/ssl/private/mysql-server.key
# will be created pointing at the private key.
#
# The symlink parameter is passed through to apache::cert::comodo and refers to
# whether or not you want apache::cert::comodo to create its own symlink
#'server.pem' pointing at the certificate. If you _do_ want such a
# symlink, be sure to specify the symlink parameter and set it to true.
#
# NOTE 1. You must provision the certificate before you can use this
# module.
#
# NOTE 2. This module sets up the MySQL server so that it _can_ use
# SSL encryption, but MySQL does not, by default, use encryption: you
# must enable encryption on a per-account-host basis. For more
# information, see https://www.stanford.edu/~adamhl/adamhlwiki/mysql/ssl/
#
# CAVEAT 1. This module has been tested on Debian squeeze and no other
# distributions.
# CAVEAT 2. The certificate to download is assumed to be from Incommon/Comodo.
# CAVEAT 3. This module does not (yet) support self-signed certificates.
#
#
# EXAMPLE:
#    mysql::server::ssl { 'tools-dev':
#      ensure       => present,
#    }
#
# This is equivalent to
#
#    mysql::server::ssl { 'tools-dev':
#      ensure => present,
#      subject => 'tools-dev',
#    }
#
# which is equivalent to
#
#    mysql::server::ssl { 'tools-dev':
#      ensure => present,
#      subject => 'tools-dev',
#      keyname => 'unix-tools-dev-ssl-key',
#      symlink => false,
#    }
#
#
#
# QUESTION: Will this work on servers with multiple MySQL services running?

define mysql::server::ssl(
  $ensure,
  $subject  = undef, # Defaults to $name
  $keyname  = undef, # Defaults to "unix-<cert_subject>-ssl-key" (FIX)
  $symlink  = false,
)
{
  if (!($ensure in ['present', 'absent'])) {
    fail "Invalid ensure value: $ensure"
  }

  # By default, we use the server name as the certificate name.
  # If you need to use a different name, a good choice is to use
  # "<hostname>-mysql. For example, if on the server tools-dev you cannot
  # use the subject "tools-dev", use "tools-dev-mysql".
  if ($subject) {
    $subject_real = $subject
  } else {
    $subject_real = $name
  }

  # Set the name of the wallet that contains the private key.
  if ($keyname) {
    $keyname_real = $keyname
  } else {
    $keyname_real = "unix-${subject_real}-ssl-key"
  }

  # Get the certificate and install the private key.
  apache::cert::comodo { $subject_real:
    ensure     => $ensure,
    keyname    => $keyname_real,
    identity   => "${subject_real}.stanford.edu",
    comodoroot => 'incommon-2020',
    symlink    => $symlink,
    notify     => Exec['mysql restart'],
  }

  # We want sym-link aliases for the key and cert file
  $ensure_link  = $ensure ? {
    present => link,
    absent  => absent,
    default => $ensure,
  }

  file { '/etc/ssl/certs/mysql-server.pem':
    ensure  => $ensure_link,
    target  => "${subject_real}.pem",
    require => Apache::Cert::Comodo[$subject_real],
  }

  file { '/etc/ssl/private/mysql-server.key':
    ensure  => $ensure_link,
    target  => "${subject_real}.key",
    require => Apache::Cert::Comodo[$subject_real],
  }

  # Need the mysql module to be included
  case $ensure {
    'present': { include mysql }
    default:   { }
  }

  # Create the mysqld_ssl.conf file
  file { '/etc/mysql/conf.d/mysqld_ssl.cnf':
    ensure  => $ensure,
    source  => 'puppet:///modules/mysql/etc/mysql/conf.d/mysqld_ssl.cnf',
    require => Package['mysql-server'],
    notify  => Exec['mysql restart'],
  }

  # So that the mysql process can read the cert, put it in the ssl-cert
  # group.
  user { 'mysql':
    ensure     => present,
    membership => minimum,
    groups     => [ 'ssl-cert' ],
    notify     => Exec['mysql restart'],
  }

  exec {'mysql restart':
    command     => '/etc/init.d/mysql restart',
    refreshonly => true,
    require     => Package['mysql-server'],
  }

}
