# modules/mysql/init.pp
#
# Offer basic classes to support mysql servers running on arbitrary systems.
# This might not match what the s_mysql servers are running in every case, but
# it's a reasonable start.

class mysql {
  case $::operatingsystem {
    'redhat': {
      package {
        'mysql-server': ensure => installed;
        'mysql':        ensure => installed;
      }
      service { 'mysqld':
        ensure    => running,
        hasstatus => true;
      }
    }

    'debian', 'ubuntu': {
      package {
        'mysql-server': ensure => installed;
        'mysql-client': ensure => installed;
      }
      service { 'mysql':
        ensure    => running,
        hasstatus => true;
      }
    }

    default: {
      fail("no case for operatingsystem '${::operatingsystem}'")
    }
  }

  file { '/etc/filter-syslog/mysql':
    source => 'puppet:///modules/mysql/etc/filter-syslog/mysql',
  }
}

