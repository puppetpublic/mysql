# Generate the MySQL configuration file(s)
#
# This Puppet define creates the file /etc/mysql/conf.d/custom.cnf where
# we put most of the MySQL confguration overrides.
#
# We also install the "stock" my.cnf in /etc/mysql since some
# distributions insist on having one (even though most of its settings are
# overridden by /etc/mysq/conf.d/custom.cnf).
#
# Notes.
#
# * If you override $log_bin_directory, make sure you value does NOT have
#   a trailing slash.
#
# * autoincrement_on: if set to true, the two configuration options
#   auto_increment_increment and auto_increment_offset will be set
#   appropriately. The default value is false.
#
# * Note that the server_id's default value is set to 0. Be sure to set
#   it explicitly if you want to use it.
#
# Definitions
define mysql::config(
  $serverid                     =       0,
  $maxconnections               =    2000,
  $maxuserconnections           =      50,
  $querycache                   =   '64M',
  $tablecache                   =    3000,
  $innodb_buffer_pool_size      = '4096M',
  $innodb_flush_method          =  'NONE',
  $innodb_io_capacity           =     200,
  $innodb_buffer_pool_instances =       1,
  $innodb_file_per_table        =       0,
  $key_buffer_size              = '2048M',
  $log_bin_directory            = '/var/log/mysql',
  $mysql_readonly               =   false,
  $autoincrement_on             =   false
){

  file {
    '/etc/mysql':        ensure => directory;
    '/etc/mysql/conf.d': ensure => directory;
  }

  file { '/etc/mysql/conf.d/custom.cnf':
      content => template('mysql/custom.cnf.erb'),
      require => Package['mysql-server'],
  }

  file { $log_bin_directory:
    ensure => directory,
    owner  => 'mysql',
    group  => 'adm',
    mode   => '2750',
  }

  # Ensure sure that the MySQL error log file is writable by the mysql
  # process.
  file { "${log_bin_directory}/mysql.err":
    ensure  => file,
    owner   => 'mysql',
    group   => 'adm',
    mode    => '0660',
  }

  # For Debian we want to install the stock my.cnf file (it gets
  # overridden by custom.cnf but we still need it).
  if ($::operatingsystem == 'debian') {
    file { '/etc/mysql/my.cnf':
      source  => "puppet:///modules/mysql/etc/mysql/my.cnf.stock.${::lsbdistcodename}",
      require => Package['mysql-server'],
    }
  }

}
